docker images --format "{{.Repository}}:{{.Tag}}" | Select-String "(:*latest*|:*alpine*|:*debian*|:*bookworm*|:*jammy*|:*lts*|:*slim*)" | ForEach-Object { docker pull $_ }
docker system prune -f
