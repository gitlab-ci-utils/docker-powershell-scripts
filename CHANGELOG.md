# Changelog

## v5.3.0 (2024-09-17)

### Changed

- Added `image-tools` script to run
  [`image-tools`](https://gitlab.com/gitlab-ci-utils/container-images/image-tools).
  (#12)
- Updated all `node-*-puppeteer-*` scripts to the new
  [`puppeteer`](https://gitlab.com/gitlab-ci-utils/container-images/puppeteer) image.
  Also updated all `node-latest-puppeteer-*` scripts to Node 22. (#13)
  - Note these images have all been upgraded to Debian Bookworm.

## v5.2.0 (2024-03-31)

### Changed

- Added `osv-scanner` script to run
  [`osv-scanner`](https://github.com/google/osv-scanner) in the current
  working directory.

## v5.1.0 (2024-03-24)

### Changed

- Added `cloc` script to run `cloc` in the current working directory.
- Updated `docker-pull-here` script to include `bookworm` and `jammy` images.
- Added `vale-here` script to open `vale` image shell in the current working
  directory.

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#11)

## v5.0.0 (2024-01-03)

### Changed

- BREAKING: Update scripts for consistent use of the `*-here` suffix, now used
  for all cases where a shell is opened with the current working directory
  mounted to a volume at `/app`.
  - Renamed `pmd-cpd-here` to `gitlab-pmd-cpd-here` for consistency.
  - Updated all `*-here` scripts to mount to the volume `/app`.
- BREAKING: Removed the `node-14-alpine` script since end-of-life.
- BREAKING: Added scripts for common scenarios that are not `*-here`, for
  example `alpine-latest`. Note these names follow the established convention,
  which may conflict with the previous versions that mounted the current
  working directory, but were not named with the `-here` suffix.
- BREAKING: Updated  `go-here` script to use `golang:latest` instead of a
  pinned version.

## v4.5.0 (2023-11-12)

### Changed

- Added `pmd-cpd-here` and `golangci-lint-here` script.

## v4.4.0 (2023-10-30)

### Changed

- Added `go-here` and `go-test-here` scripts.
- Added `node-latest-puppeteer` and `node-latest-puppeteer-root` scripts, the
  same as the `lts` equivalents, but running Node current (v21).
- Added `node-lts-puppeteer-privileged` script.

## v4.3.0 (2023-08-20)

### Changed

- Added `ubuntu-latest` script to run the latest Ubuntu image in the current
  working directory.

## v4.2.0 (2023-08-11)

### Changed

- Added `playwright-jammy` script to run 'playwright' in the current working
  directory. Runs with `CI=true` to emulate CI environments. (#9)

### Fixed

- Updated all `*puppeteer*` scripts to set `CI=true` to emulate CI
  environments.
- Added `.editorconfig` for consistent file formatting.

## v4.1.0 (2023-04-27)

### Changed

- Updated `hadolint` script to use latest Alpine image. (#7)

## v4.0.0 (2022-05-12)

### Changed

- BREAKING: Updated `config-files` script to reference new project location. (#5)

## v3.0.0 (2021-11-16)

### Changed

- BREAKING: Removed `main` tag from `docker-pull-latest` as workaround longer required

## v2.0.1 (2021-11-12)

### Changed

- Updated `config-files` to pull image from GitLab repo

## v2.0.0 (2021-11-11)

### Added

- Added `node-14-alpine` script to facilitate v1 `package-lock.json` updates

### Changed

- BREAKING: Updated node `latest` scripts to node 17
- Updated tags for `docker-pull-latest` script to include `main`

### Miscellaneous

- Integrate [renovate](https://docs.renovatebot.com/) for automatic dependency updates (#3)

## v1.0.0 (2021-09-17)

Initial versioned release
