# Docker PowerShell scripts

A collection of PowerShell scripts to launch containers with Docker Desktop in
Windows.

Scripts follow the general convention of
`<image>-<version>-<modifiers>.ps1` or `<image>-<version>-<modifiers>-here.ps1`, where:

- `<image>` is generally the name of the image, for example `node` or
  `curl-jq`. Note the name of the image may be `-` delimited.
- `<version>` is the version of the image, which could also contain the OS,
  for example `lts`, `lts-alpine`, `jammy`, or `latest`.
- `<modifiers>` is a list of applicable modifiers, which could be the user or
  other meaningful information, for example:
  - `root` for root user.
  - `puppeteer` for an image compatible with running Puppeteer.
- `-here` is a suffix to indicate that the script opens a shell with the
  current working directory mounted to a volume at `/app`.
